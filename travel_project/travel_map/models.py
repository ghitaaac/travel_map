from django.db import models
from django_countries.fields import CountryField


class SelectedCountries(models.Model):
    country = CountryField(blank_label='(select country)')
    selected_date = models.DateField()


class GeoLocation(models.Model):
    country_code = models.CharField(max_length=100)
    country_name = models.CharField(max_length=100)
    latitude = models.IntegerField()
    longitude = models.IntegerField()
