from django.shortcuts import render
from .models import SelectedCountries, GeoLocation


def geo_locations(request):
    countries = SelectedCountries.objects.all()
    latitudes = []
    longitudes = []
    geo_infos = []
    for country in countries:
        geo_info = GeoLocation.objects.get(country_name=country.country.name)
        geo_infos.append(geo_info)
        latitude = geo_info.latitude
        latitudes.append(latitude)
        longitude = geo_info.longitude
        longitudes.append(longitude)

    return render(request, 'main_map.html', {'latitudes': latitudes, 'longitudes': longitudes, 'geo_infos' : geo_infos})

def home(request):
    return render(request, 'home.html')
