from django.contrib import admin
from .models import SelectedCountries

@admin.register(SelectedCountries)
class CountriesAdmin(admin.ModelAdmin):
    list_display = ['id', 'country', 'selected_date']